class Ground{
  String image_path = "background/layer_floor.png";
  
  PImage ground1;
  PImage ground2;
  
  float ground1_x = 0;
  float ground1_y = 0;
  
  float ground2_x = width;
  float ground2_y = 0;
  
  Ground(){
    ground1 = loadImage(image_path);
    ground2 = loadImage(image_path);
  }
  
  void drawing(){
    image(ground1, ground1_x, ground1_y, width, height);
    image(ground2, ground2_x, ground2_y, width, height);
  }
  
  void act(){
    ground1_x = ground1_x - speed;
    ground2_x = ground2_x - speed;
    
    if(ground1_x <= -width){
      ground1_x = width + ground2_x;
    }
    
    if(ground2_x <= -width){
      ground2_x = width + ground1_x;
    }
  }
 
}