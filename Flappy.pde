class Flappy{
  String image_path[] = {  "flappy/frame-1.png",
                           "flappy/frame-2.png",
                           "flappy/frame-3.png",
                           "flappy/frame-4.png"
                        };
                        
  float size = 0.4f;
  float animation_time = 0.2f;
  float gravitation = 5;
  float jump = 40;
                        
  PImage flappy[];
  float flappy_x;
  float flappy_y;
  float image_width;
  float image_height;
  int animation;
  float time;
  float y_velocity;
  
  Flappy(){
    animation = 0;
    time = 0;
    flappy = new PImage[image_path.length];
    for(int i = 0 ; i < image_path.length; i++){
      flappy[i] = loadImage(image_path[i]); 
    }
    
    y_velocity = 0;
    
    flappy_x = start_flappy_x;
    flappy_y = start_flappy_y;
  }
  
  void drawing(){
    image_width = flappy[0].width * width / background.background.width * size;
    image_height = flappy[0].height * height / background.background.height * size;
    image(flappy[animation], flappy_x, flappy_y, image_width, image_height); 
  }
  
  void act(){
    y_velocity = y_velocity + gravitation;
    flappy_y = flappy_y + y_velocity;
    
    float delta_time = 1 / frameRate;
    time = time + delta_time;
    
    if(time >= animation_time){
      animation++;
      if(animation >= image_path.length){
        animation = 0;
      }
    }
  }  
  
  void jump(){
    y_velocity = -jump;
  }
}