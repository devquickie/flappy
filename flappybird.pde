float speed = 20;
float gap = 250;
int spike_number = 2;
float start_flappy_x = 100;
float start_flappy_y = 100;

Background background;
Ground ground;
Spike spike[];
Flappy flappy;

void setup(){
  size(1024, 768);
  background = new Background();
  ground = new Ground();
  spike = new Spike[spike_number];
  for(int i = 0; i < spike_number; i++){
    spike[i] = new Spike(i);
  }
  flappy = new Flappy();
}

void draw(){
  background.drawing();
  
  for(int i = 0; i < spike_number; i++){
    spike[i].drawing();
    spike[i].act();
    if(spike[i].collition(flappy.flappy_x, flappy.flappy_y, flappy.image_width, flappy.image_height)){
      println("collition: " + i);  
    }
  }
  
  ground.drawing();
  ground.act();
  
  flappy.drawing();
  flappy.act();
}

void keyReleased(){
  if(key == ' '){ 
    flappy.jump();
  }
}