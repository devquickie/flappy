class Spike{
  String image_path1 = "spikes/spike.png";
  String image_path2 = "spikes/spike_f.png";
  
  PImage top_spike;
  PImage bottom_spike;
  
  float spike_x;
  float spike_y;
  float image_width;
  float image_height;
  
  Spike(int number){
    bottom_spike = loadImage(image_path1);
    top_spike = loadImage(image_path2);
    spike_x = width / spike_number * (number + 1);
    setY();
  }
  
  void drawing(){
    image_width = bottom_spike.width * width / background.background.width;
    image_height = bottom_spike.height * height / background.background.height;
    image(bottom_spike, spike_x, spike_y + gap/2, image_width, image_height);  
    image(top_spike, spike_x, spike_y - gap/2 - image_height, image_width, image_height);
  }
  
  void setY(){
    spike_y = random(height / 7, height / 10 * 6);
  }
  
  void act(){
    spike_x = spike_x - speed;
    
    if(spike_x <= -image_width){
      spike_x = width;
      setY();
    }
  }
  
  boolean collition(float object_x, float object_y, float object_width, float object_height){

    if( object_x < spike_x + image_width &&               // top spike
        object_x + object_width > spike_x &&
        object_y < spike_y - gap/2 &&
        object_y + object_height > spike_y - gap/2 - image_height){
          
      println("collition top");  
      return true;
    }else if( object_x < spike_x + image_width &&         // bottom spike
        object_x + object_width > spike_x &&
        object_y < spike_y + gap/2 + image_height &&
        object_y + object_height > spike_y + gap/2){
      
      println("collition bottom");
      return true;
    }else{
      return false;  
    }
  }
  
}